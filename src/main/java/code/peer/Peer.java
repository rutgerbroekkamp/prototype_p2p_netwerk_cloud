package code.peer;

import code.ConnectionInfo;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Peer {
    private List<Socket> sockets = new ArrayList<>();
    private List<ConnectionInfo> connections = new ArrayList<>();
    private boolean stopPingListen = false;

    public Peer() {
        boolean userIsInitialPeer = readIfUserIsInitialPeer();
        int localPort = readPortNumberForLocalServer();

        if (!userIsInitialPeer) {
            String initialConnectionIpAddress = readIPForFirstConnection();
            int initialConnectionPortNumber = readPortNumberForFirstConnection();

            ConnectionInfo connectionInfo = new ConnectionInfo(initialConnectionIpAddress, initialConnectionPortNumber);
            connections.add(connectionInfo);

            createConnectionWithPeer(connectionInfo);
            sharePortWithPeer(sockets.get(0), localPort);
            readConnectionsFromPeer(sockets.get(0));
            connectWithNewPeers();
        }

        startListeningForConnections(localPort);
        //TODO, dit werkt bijna, initial peer krijgt geen pings en er is nog een onafgevangen exception.
//        sendPingsToPeers();
//        startListeningForPings();
    }

    private void connectWithNewPeers() {
        List<Socket> receivedSockets = new ArrayList<>();
        for (ConnectionInfo connection : connections) {
            for (Socket socket : sockets) {
                if (!(socket.getInetAddress().toString().equals("/" + connection.getIpAddress()) && (socket.getPort() == connection.getPort()))) {
                    System.out.println("Setting up new connection with: " + connection.getIpAddress());
                    try {
                        receivedSockets.add(new Socket(connection.getIpAddress(), connection.getPort()));
                    } catch (IOException e) {
                        System.out.println("Error: could not setup connection with: " + connection.getIpAddress());
                    }
                }
            }
        }
        sockets = Stream.concat(receivedSockets.stream(), sockets.stream())
                .collect(Collectors.toList());
    }

    private boolean readIfUserIsInitialPeer() {
        System.out.println("Are you the initial peer? Type 'true' if so, else type 'false':");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextBoolean();
    }

    private String readIPForFirstConnection() {
        System.out.println("Provide the IP address for the first connection:");
        Scanner scanner = new Scanner(System.in);
        String ipAddress = scanner.next();

        return ipAddress;
    }

    private int readPortNumberForFirstConnection() {
        System.out.println("Provide the port number for the first connection.");
        Scanner scanner = new Scanner(System.in);

        return scanner.nextInt();
    }

    private int readPortNumberForLocalServer() {
        System.out.println("Provide the port number to host your server.");
        Scanner scanner = new Scanner(System.in);

        return scanner.nextInt();
    }

    private void createConnectionWithPeer(ConnectionInfo connectionInfo) {
        try {
            sockets.add(new Socket(connectionInfo.getIpAddress(), connectionInfo.getPort()));
            System.out.println("Connected to: " + connectionInfo.getIpAddress());
        } catch (IOException e) {
            System.out.println("Error: could not connect to: " + connectionInfo.getIpAddress());
        }
    }

    private void startListeningForConnections(int localPort) {
        Runnable serverTask = () ->
        {
            try {
                ServerSocket serverSocket = new ServerSocket(localPort);
                System.out.println("Waiting for clients to connect in separate thread...");

                while (true) {
                    Socket newSocket = serverSocket.accept();
                    System.out.println("Connected to: " + newSocket.getInetAddress().toString());

                    shareConnectionsWithPeer(newSocket);
                    int newPort = readPortFromPeer(newSocket);
                    System.out.println("\t on port: " + newPort);
                    connections.add(new ConnectionInfo(newSocket.getInetAddress().toString().substring(1), newPort));
                    sockets.add(newSocket);
                }
            } catch (IOException e) {
                System.err.println("Error: could not connect to: " + sockets.get(sockets.size() - 1).getInetAddress().toString());
            }
        };

        Thread serverThread = new Thread(serverTask);
        serverThread.start();
    }

    private void sendPingsToPeers() {
        Runnable sendPingTask = () ->
        {
            long timeOfLastPing = System.currentTimeMillis();
            while (true) {
                if (timeOfLastPing < System.currentTimeMillis() - 5000) { //Send a ping every 5000 milliseconds.
                    sendPingToConnectedPeers();
                    timeOfLastPing = System.currentTimeMillis();
                }
            }
        };

        Thread sendPingThread = new Thread(sendPingTask);
        sendPingThread.start();
    }

    private void sendPingToConnectedPeers() {
        for (Socket socket : sockets) {
            try {
                DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
                outputStream.writeInt(-1);
            } catch (IOException e) { //Might be a very good place to check if we lost connection with a peer.
                System.out.println("Error: failed to send ping to " + socket.getInetAddress().toString());
                sockets.remove(socket);
            }
        }
    }

    private void startListeningForPings() {
        Runnable listenForPingTask = () ->
        {
            while (true) {
                if (!stopPingListen) {
                    for (Socket socket : sockets) {
                        try {
                            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                            if (dataInputStream.available() > 0 && dataInputStream.readInt() == -1) {
                                System.out.println("Received ping from: " + socket.getInetAddress().toString() + ":" + socket.getPort() + " - " + LocalTime.now().toString());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };

        Thread pingListenerThread = new Thread(listenForPingTask);
        pingListenerThread.start();
    }

    private void sharePortWithPeer(Socket socket, int localPort) {
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataOutputStream.writeInt(localPort);
        } catch (IOException e) {
            System.out.println("Error: could not share port with peer");
        }
    }

    private int readPortFromPeer(Socket socket) {
        try {
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            return dataInputStream.readInt();
        } catch (Exception e) {
            System.out.println("Error: could not read port from peer");
            return 0;
        }
    }

    private void shareConnectionsWithPeer(Socket socket) {
        try {
            System.out.println("\t Sharing known connections with: " + socket.getInetAddress().toString());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            objectOutputStream.writeObject(connections);
        } catch (IOException e) {
            System.out.println("Error: could not share connections with peer");
        }
    }

    private void readConnectionsFromPeer(Socket socket) {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            List<ConnectionInfo> newConnections = (List<ConnectionInfo>) objectInputStream.readObject();

            for (ConnectionInfo newConnection : newConnections) {
                if (!connections.contains(newConnection)) {
                    newConnection.setIpAddress(newConnection.getIpAddress());
                    connections.add(newConnection);
                }
                System.out.println("Connection received - IPAddress: " + newConnection.getIpAddress() + ", port: " + newConnection.getPort());
            }
        } catch (Exception e) {
            System.out.println("Error: could not read connections from peer");
        }
    }
}
